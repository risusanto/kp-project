-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: May 12, 2018 at 01:35 PM
-- Server version: 5.7.21-log
-- PHP Version: 7.0.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `seo_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `username` varchar(36) NOT NULL,
  `password` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `document`
--

CREATE TABLE `document` (
  `id_document` bigint(20) NOT NULL,
  `content` text,
  `isi` text,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `judul` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `document`
--

INSERT INTO `document` (`id_document`, `content`, `isi`, `tanggal`, `judul`) VALUES
(3, ' Pernahkah Ketika Anda menekan tombol power laptop Anda dan Anda melihat lampu daya hidup, kipas berjalan, harddisk berjalan normal tetapi tidak ada yang muncul di layar dan layar menampilkan tampilan kosong/gelap, Ini adalah apa yang kita sebut \"no display/tidak ada tampilan\" .\r\n\r\nDalam masalah ini mungkin laptop Anda harus memberikan catu daya yang tepat untuk semua bagian hardware tetapi motherboard tidak bisa boot karena beberapa kegagalan hardware atau boot motherboard Anda normal tapi tidak mampu menunjukkan tampilan hanya karena layar laptop anda rusak .\r\n\r\nSalah satu cara yang dapat Anda lakukan untuk memecahkan masalah ini adalah Anda harus melakukan langkah demi langkah pemeriksaan Hardware laptop Anda untuk memperbaiki masalah ini. \r\n\r\nDalam tutorial ini saya akan menunjukkan Anda bagaimana Anda dapat dengan mudah memecahkan masalah \"no display\" anda, dengan mengikuti beberapa langkah sederhana yang akan kami tunjukan kepada anda berikut.\r\n\r\nJika laptop Anda menunjukkan tanda-tanda power supply bermasalah atau dengan kata lain sudah tidak mampu mensuplay daya karena sudah lama digunakan, Anda dapat mencoba menggunakan adaptor daya yang berbeda atau memeriksa daya yang masuk dengan alat ukur daya. ', ' Untuk memperbaiki masalah layar kosong dan hitam ada 3 hal yang mungkin dapat Anda lakukan untuk memperbaiki laptop Anda. Jadi, kita akan melakukan langkah demi langkah pemeriksaan untuk memperbaiki laptop Anda agar normal kembali.\r\n\r\nTampilan/Display\r\n\r\nPertama kita mulai dengan layar, kadang-kadang masalahnya adalah hanya karena tampilan layar anda rusak. Jika tampilan Anda mati atau rusak motherboard Anda tidak akan pernah bisa menunjukkan sesuatu di layar atau memberikan beberapa kode kesalahan. Anda harus memecahkan masalah ini sendiri.\r\n\r\nUntuk memecahkan masalah ini Anda memerlukan sebuah layar eksternal untuk memeriksa apakah laptop Anda bekerja dengan baik atau tidak. Di sini kita memiliki tutorial lain untuk menunjukkan cara untuk melakukan  Cara menghubungkan Layar Laptop Rusak ke Monitor Eksternal.\r\n\r\nJika menggunakan layar eksternal masih belum berhasil, berarti Anda memiliki masalah dengan tampilan layar Anda, Anda harus menggantinya dengan yang baru untuk memperbaiki laptop Anda atau jika laptop Anda masih tidak menunjukkan apa pun di layar, jika layar eksternal dapat menampilkan display, maka yang bermasalah adalah layar internal anda, untuk itu Anda dapat beralih ke solusi berikutnya di bawah ini.\r\n\r\nCek bios\r\n\r\nKadang-kadang bios sistem kami berhenti bekerja karena beberapa alasan dan bios perlu di restart untuk berjalan dengan baik kembali.Untuk mereset bios laptop Anda ikuti langkah-langkah di bawah ini :\r\n\r\nLangkah 1 - matikan laptop dan cabut adaptor daya.', '2018-05-01 19:26:49', 'Layar Laptop Mati'),
(6, '<p>Gangguan pada VGA Card/Layar Monitor. Masalah seperti tanda-tanda gangguan yang terjadi pada VGA Card di antaranya adalah:</p>\r\n\r\n<ol>\r\n	<li>Komputer menjadi macet atau hang ketika digunakan untuk bermain game 3D.</li>\r\n	<li>Tidak dapat digunakan untuk menjalankan permainan tertentu.</li>\r\n	<li>Windows tidak bisa digunakan dalam mode normal.</li>\r\n	<li>Ada titik-titik kecil di layar monitor.</li>\r\n</ol>\r\n', '<p>Cek kabel VGA yang terhubung ke monitor dan ke casing komputer, apakah sudah menancap dengan benar atau belum, kalau belum masukkan kabel tersebut sesuai dengan kaki-kaki yang tersedia. Cek konektor atau pin yang terdapat pada kabel VGA. Kalau ada yang patah atau putus, segera ganti yang baru. Install kembali atau perbaharui driver dari VGA Card yang dipasang. Untuk mendapatkan driver VGA tersebut, bisa dengan men-download dari situs yang menyediakan driver dari VGA itu. Cobalah untuk mendownload versi terbaru versi terakhir software Direct-X dari situs Microsoft. Kunjungi situs-situs yang menangani berbagai permasalahan tentang permainan atau games. Buka casing komputer, cabut VGA card dari slotnya (PC/AGP) dari motherboard, dan pasang kembali. Apabila pada waktu komputer dihidupkan, layar masih hitam dan belum muncul gambarnya, ganti VGA card dengan yang baru, karena dapat dipastikan VGA card ada mengalami kerusakan.</p>\r\n', '2018-05-01 20:02:49', 'Kerusakan Pada VGA');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `document`
--
ALTER TABLE `document`
  ADD PRIMARY KEY (`id_document`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `document`
--
ALTER TABLE `document`
  MODIFY `id_document` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
