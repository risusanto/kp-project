<div class="content-wrapper">
    <div class="container">
      <!-- Main content -->
      <section class="content">
        <div class="box box-default">
          <div class="box-body">
            <?=form_open('cari',['method' => 'get'])?>
            <div class="form-group col-md-8">
              <input type="text" class="form-control" name="q" value="<?=$this->input->get('q')?>">
            </div>
            <div class="form-group col-md-4">
              <input type="submit" class="btn bg-blue" name="" value="Cari">
            </div>
            <?=form_close()?>
            <div class="container">
            <small>Mendapatkan <strong><?=sizeof($result)?></strong> dokumen dalam <strong>{elapsed_time}s</strong></small>
            </div>
          </div>
          <!-- /.box-body -->
        </div>
        <?php foreach ($result as $key => $value): ?>
          <?php $konten = $this->Document_m->get_row(['id_document' => $key]) ?>
          <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title"><?=$konten->judul?></h3>
            </div>
            <div class="box-body">
              <?=substr($konten->isi,0,500)?>...<br>
              <a href="<?=base_url('home/detail-dokumen?id_dokumen='.$key)?>" target="_blank">Selengkapnya</a>
            </div>
            <!-- /.box-body -->
          </div>
        <?php endforeach; ?>
        <!-- /.box -->
      </section>
      <!-- /.content -->
    </div>
    <!-- /.container -->
  </div>
