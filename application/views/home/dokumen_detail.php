<div class="content-wrapper">
    <div class="container">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          <?=$dokumen->judul?>
        </h1>
      </section>

      <!-- Main content -->
      <section class="content">
        <div class="box box-default">
          <div class="box-header with-border">
            <h3 class="box-title">Gejala Kerusakan</h3>
          </div>
          <div class="box-body">
            <?=$dokumen->content?>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
        <div class="box box-default">
          <div class="box-header with-border">
            <h3 class="box-title">Penjelasan/Solusi</h3>
          </div>
          <div class="box-body">
            <?=$dokumen->isi?>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </section>
      <!-- /.content -->
    </div>
    <!-- /.container -->
  </div>
