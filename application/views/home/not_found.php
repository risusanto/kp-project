<!-- Full Width Column -->
<div class="content-wrapper">
  <div class="container">
    <!-- Content Header (Page header) -->
    <section class="content-header">

    </section>

    <!-- Main content -->
    <div class="box box-default">
      <div class="box-body">
        <?=form_open('cari',['method' => 'get'])?>
        <div class="form-group col-md-8">
          <input type="text" class="form-control" name="q" value="<?=$this->input->get('q')?>">
        </div>
        <div class="form-group col-md-4">
          <input type="submit" class="btn bg-blue" name="" value="Cari">
        </div>
        <?=form_close()?>
      </div>
      <!-- /.box-body -->
    </div>
    <section class="content">
      <div class="callout callout-danger">
          <h4>Tidak Ditemukan!</h4>

          <p>Hasil pencarian untuk "<strong><?=$this->input->get('q')?></strong>" tidak ditemukan.</p>
        </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.container -->
</div>
<!-- /.content-wrapper -->
