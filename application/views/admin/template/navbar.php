<header class="main-header">
    <!-- Logo -->
    <a href="index2.html" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>IS</b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg">IS <b>Telkom</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="<?=base_url('admin/logout')?>">
              <img src="<?=base_url('assets/')?>dist/img/user-160x160.jpg" class="user-image" alt="User Image">
              <span class="hidden-xs">Admin | Logout</span>
            </a>
          </li>
        </ul>
      </div>
    </nav>
  </header>
