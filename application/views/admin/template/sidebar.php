<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?=base_url('assets/')?>dist/img/user-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Admin</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li id="dashboard"><a href="<?=base_url('admin')?>"><i class="fa fa-laptop"></i> <span>Dashboard</span></a></li>
        <li id="dokumen_menu" class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Dokumen</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?=base_url('admin/tambah_dokumen')?>"><i class="fa fa-circle-o"></i> Tambah Baru</a></li>
            <li><a href="<?=base_url('admin/dokumen')?>"><i class="fa fa-circle-o"></i> Kelola Dokumen</a></li>
          </ul>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
