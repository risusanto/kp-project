<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Admin
        <small>Kelola Dokumen</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Kelola Dokumen</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Daftar Dokumen</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="daftar_dok" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No.</th>
                  <th>Judul</th>
                  <th>Tanggal</th>
                  <th width="40px">Pilihan</th>
                </tr>
                </thead>
                <tbody>
                  <?php $i = 1; foreach ($data as $dokumen): ?>
                    <tr>
                      <td><?=$i++?></td>
                      <td><?=$dokumen->judul?></td>
                      <td><?=$dokumen->tanggal?></td>
                      <td>
                        <a href="<?=base_url('admin/edit-dokumen?id='.$dokumen->id_document)?>" class="btn btn-flat bg-blue fa fa-pencil">
                        </a>
                        <a href="<?=base_url('home/detail-dokumen?id_dokumen='.$dokumen->id_document)?>" class="btn btn-flat bg-green fa fa-eye" target="_blank">
                        </a>
                        <button type="button" class="btn btn-flat bg-red fa fa-trash" onclick="hapus(<?=$dokumen->id_document?>)"></button>
                      </td>
                    </tr>
                  <?php endforeach; ?>
                </tbody>
                <tfoot>
                <tr>
                  <th>No.</th>
                  <th>Judul</th>
                  <th>Tanggal</th>
                  <th width="240px">Pilihan</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- DataTables -->
  <link rel="stylesheet" href="<?=base_url('assets')?>/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <script src="<?=base_url('assets')?>/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
  <script src="<?=base_url('assets')?>/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
  <script>
    $('#daftar_dok').DataTable()
  </script>
  <script type="text/javascript">
    function hapus(id) {
      swal({
          title: "Hapus Dokumen?",
          text: "Ya, untuk hapus",
          icon: "warning",
          buttons: {
            cancel: "Batal",
            confirm:"Ya"
          },
          dangerMode: true
        })
        .then((konfirm) => {
          if (konfirm) {
            $.ajax({
                url: "<?= base_url('admin/dokumen') ?>",
                type: 'POST',
                data: {
                    id: id,
                    hapus_doc: true
                },
                success: function() {
                  location.reload();
                }
            });
          }
        });
    }
  </script>
