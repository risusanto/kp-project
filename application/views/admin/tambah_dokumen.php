<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Admin
        <small>Tambah Dokumen</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?=base_url('admin')?>"><i class="fa fa-laptop"></i> Dashboard</a></li>
        <li class="active">Tambah dokumen</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-info">
            <div class="box-header">
              <!-- tools box -->
              <div class="pull-right box-tools">
                <button type="button" class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip"
                        title="Collapse">
                  <i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-info btn-sm" data-widget="remove" data-toggle="tooltip"
                        title="Remove">
                  <i class="fa fa-times"></i></button>
              </div>
              <!-- /. tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body pad">
              <?=form_open('admin/tambah-dokumen')?>
                    <div class="form-group">
                      <label for="[object Object]">Judul:</label>
                      <input type="text" class="form-control" name="judul" value="">
                    </div>
                    <div class="form-group">
                      <label for="[object Object]">Gejala:</label>
                      <textarea id="dokumen1" rows="10" cols="80" name="content"></textarea>
                    </div>
                    <div class="form-group">
                      <label for="[object Object]">Solusi:</label>
                      <textarea id="dokumen" rows="10" cols="80" name="isi"></textarea>
                    </div>
                    <div class="form-group">
                      <input type="submit" class="btn bg-olive"  name="simpan" value="SIMPAN">
                    </div>
              <?=form_close()?>
            </div>
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col-->
      </div>
      <!-- ./row -->
    </section>
    <!-- /.content -->
  </div>
  <script src="<?=base_url('assets')?>/bower_components/ckeditor/ckeditor.js"></script>
  <script>
  $(function () {
    CKEDITOR.replace('dokumen')
    CKEDITOR.replace('dokumen1')
  })
</script>
