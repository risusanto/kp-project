<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
     <h1>
       Admin
       <small>Dashboard</small>
     </h1>
     <ol class="breadcrumb">
       <li><a href="#"><i class="fa fa-laptop"></i> Dashboard</a></li>
     </ol>
   </section>

   <!-- Main content -->
   <section class="content">
     <!-- Small boxes (Stat box) -->
     <div class="row">
       <div class="col-lg-3 col-xs-6">
         <!-- small box -->
         <div class="small-box bg-aqua">
           <div class="inner">
             <?php
             $doc = $this->Document_m->get();
              ?>
             <h3><?=count($doc)?></h3>

             <p>Dokumen Panduan</p>
           </div>
           <div class="icon">
             <i class="ion ion-bag"></i>
           </div>
           <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
         </div>
       </div>
       <!-- ./col -->
     </div>
     <!-- /.row -->
   </section>
   <!-- /.content -->
 </div>
