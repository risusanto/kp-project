<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 *
 */
class Admin_m extends MY_Model
{

  function __construct()
  {
    $this->data['table_name'] = 'admin';
    $this->data['primary_key'] = 'username';
  }

  public function check($data)
  {
    $user = $this->get_row(['username' => $data['username'],'password' => md5($data['password'])]);
    if (isset($user)) {
      $this->session->set_userdata([
				'username'		=> $user->username,
			]);
      return true;
    }
    return false;
  }
}
