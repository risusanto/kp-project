<?php

class MY_Controller extends CI_Controller
{
  	public $title = ' | SEO IS Telkom Palembang';

	public function __construct()
	{
		parent::__construct();
		// $this->load->library('lib_log');
		date_default_timezone_set("Asia/Jakarta");
		header('Access-Control-Allow-Origin: *');
		if (isset($_SERVER['HTTP_ORIGIN']))
        {
            header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
            header('Access-Control-Allow-Credentials: true');
            header('Access-Control-Max-Age: 86400'); // cache for 1 day
        }
        $this->data['plain'] = '';
        // Access-Control headers are received during OPTIONS requests
        if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS')
        {
            if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
            	header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
            if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
            	header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
            exit(0);
        }
	}

	public function template($data,$role='')
	{
		if ($role != '') {
			return $this->load->view($role.'/template/layout', $data);
		}
		else{
			return $this->load->view('home/template/layout', $data);
		}

	}

	public function getJSON($url)
	{
		$content = file_get_contents($url);
		$data = json_decode($content);
		return $data;
	}

	public function POST($name)
	{
		return $this->input->post($name);
	}

	public function GET($name, $clean = false)
	{
		return $this->input->get($name, $clean);
	}

	public function METHOD()
	{
		return $this->input->method();
	}

	public function flashmsg($msg, $type = 'success',$name='msg')
	{
		return $this->session->set_flashdata($name, '<div class="alert alert-'.$type.' alert-dismissable"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'.$msg.'</div>');
	}

	public function upload($id, $directory, $tag_name = 'userfile')
	{
		if ($_FILES[$tag_name])
		{
			$upload_path = realpath(APPPATH. '../assets/' . $directory . '/');
			@unlink($upload_path . '/' . $id . '.jpg');
			$config = [
				'file_name' 		=> $id . '.jpg',
				'allowed_types'		=> 'jpg|png|bmp|jpeg',
				'upload_path'		=> $upload_path
			];
			$this->load->library('upload');
			$this->upload->initialize($config);
			return $this->upload->do_upload($tag_name);
		}
		return FALSE;
	}

  public function upload_any_type($id, $directory, $tag_name = 'userfile')
  {
    if ($_FILES[$tag_name])
    {
      // ini_set('upload_max_filesize', '300M');
      $upload_path = realpath(APPPATH . '../assets/uploads/' . $directory . '/');
      // @unlink($upload_path . '/' . $id);
      $config = [
        'file_name'			=> $id,
        'allowed_types'		=> '*',
        'upload_path'		=> $upload_path
      ];
      $this->load->library('upload');
      $this->upload->initialize($config);
      return $this->upload->do_upload($tag_name);
    }
    return FALSE;
  }

	public function dump($var)
	{
		echo '<pre>';
		var_dump($var);
		echo '</pre>';
	}

	public function __apiCall($url, $post_parameters = FALSE) {
     	// Initialize the cURL session
		$curl_session = curl_init();

		// Set the URL of api call
		curl_setopt($curl_session, CURLOPT_URL, $url);

		// If there are post fields add them to the call
		if($post_parameters !== FALSE) {
		curl_setopt ($curl_session, CURLOPT_POSTFIELDS, $post_parameters);
		}

		// Return the curl results to a variable
		curl_setopt($curl_session, CURLOPT_RETURNTRANSFER, 1);

		// Execute the cURL session
		$contents = curl_exec ($curl_session);

		// Close cURL session
		curl_close ($curl_session);

		// Return the response
		return json_decode($contents);

    }

    public function curl_get_request($url)
	{
		//  Initiate curl
		$ch = curl_init();
		// Disable SSL verification
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		// Will return the response, if false it print the response
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		// Set the url
		curl_setopt($ch, CURLOPT_URL,$url);
		// Execute
		$result=curl_exec($ch);
		// Closing
		curl_close($ch);

		// Will dump a beauty json :3
		$arr = json_decode($result);
		// $this->dump($arr);
		// exit;
		return $arr;
	}

	public function curl_post_request($url, $params)
	{
		//  Initiate curl
		$ch = curl_init();
		// Disable SSL verification
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		// Will return the response, if false it print the response
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		// Set the url
		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_POST, 1);

		$param = ''; $i = 0;
		foreach ($params as $key => $value)
		{
			$param .= $key . '=' . $value;
			if ($i < count($params) - 1)
			{
				$param .= '&';
				$i++;
			}
		}

		curl_setopt($ch, CURLOPT_POSTFIELDS, $param);

		// Execute
		$result=curl_exec($ch);

		// Closing
		curl_close($ch);

		// Will dump a beauty json :3
		$arr = json_decode($result);
		return $arr;
	}
}
