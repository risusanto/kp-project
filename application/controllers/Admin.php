<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 *
 */
class Admin extends MY_Controller
{

  function __construct()
  {
    parent::__construct();
    $username = $this->session->userdata('username');
    if(!isset($username)){
      $this->flashmsg('Login untuk melanjutkan','warning');
      redirect('login');
      exit;
    }
    $this->load->model('Document_m');
  }

  public function logout()
  {
    $this->session->sess_destroy();
		redirect('login');
		exit;
  }

  public function index()
  {
    $this->data['title'] = 'Dashboard'.$this->title;
    $this->data['content'] = 'admin/dashboard';
    $this->data['active'] = 'dashboard';
    $this->template($this->data,'admin');
  }
  public function tambah_dokumen()
  {
    if ($this->POST('simpan')) {
      $data = [
        'judul' => $this->POST('judul'),
        'content' => $this->POST('content'),
        'isi' => $this->POST('isi')
      ];
      $this->Document_m->insert($data);
      $this->flashmsg('Dokumen berhasil disimpan');
      redirect('admin/dokumen');
      exit;
    }
    $this->data['title'] = 'Tambah Dokumen'.$this->title;
    $this->data['content'] = 'admin/tambah_dokumen';
    $this->data['active'] = 'dokumen_menu';
    $this->template($this->data,'admin');
  }

  public function dokumen()
  {
    if ($this->POST('hapus_doc')) {
      $this->Document_m->delete($this->POST('id'));
      exit;
    }
    $this->data['data'] = $this->Document_m->get();
    $this->data['title'] = 'Daftar Dokumen'.$this->title;
    $this->data['content'] = 'admin/dokumen';
    $this->data['active'] = 'dokumen_menu';
    $this->template($this->data,'admin');
  }

  public function edit_dokumen()
  {
    if ($this->POST('simpan')) {
      $data = [
        'judul' => $this->POST('judul'),
        'content' => $this->POST('content'),
        'isi' => $this->POST('isi')
      ];
      $this->Document_m->update($this->POST('id'),$data);
      redirect('admin/dokumen');
      exit;
    }
    $id = $this->input->get('id');
    $dokumen = $this->Document_m->get_row(['id_document' => $id]);
    if(!isset($id) || !isset($dokumen)){
      redirect('admin/dokumen');
      exit;
    }
    $this->data['dokumen'] = $dokumen;
    $this->data['title'] = 'Edit dokumen: '.$dokumen->judul.$this->title;
    $this->data['content'] = 'admin/edit_dokumen';
    $this->data['active'] = 'dokumen_menu';
    $this->template($this->data,'admin');
  }

}
