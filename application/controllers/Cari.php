<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 *
 */
class Cari extends MY_Controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->library('WordPreprocessing');
    $this->load->library('VectorSpaceModel');

    $this->load->model('Document_m');
  }

  public function index()
  {
    $query = $this->input->get('q');
    if (!isset($query) || $query == NULL){
      redirect('home');
      exit;
    }
    $stopWordrm = new WordPreprocessing();
    $documents = $this->Document_m->get();
    $_terms = $stopWordrm->removeStopWords($query);

    $terms = $stopWordrm->wordTokenizing($_terms);

    $vsm = new VectorSpaceModel();
    $vsm->init($documents,$terms);
    arsort($vsm->cosSimiliarity);
    $_resultSorted = $vsm->cosSimiliarity;
    $this->data['result'] = [];
    foreach ($_resultSorted as $key => $value) {
      if ($value > 0) {
        $this->data['result'][$key] = $value;
      }
    }

    $this->data['title'] = 'Hasil pencarian untuk: '.$query;
    $this->data['content'] = 'home/result';
    //$this->dump($this->data['result']);
    if(count($this->data['result']) == 0){
      $this->data['content'] = 'home/not_found';
    }
    $this->template($this->data);
  }

}
