<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 *
 */
class Login extends MY_Controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->model('Admin_m');
    $username = $this->session->userdata('username');
    if (isset($username)) {
      redirect('admin');
      exit;
    }
  }

  public function index()
  {
    if ($this->POST('login')) {
      $data_login = [
        'username' => $this->POST('username'),
        'password' => $this->POST('password')
      ];
      if(!$this->Admin_m->check($data_login)){
        $this->flashmsg('Username atau password salah!','danger');
      }
      redirect('login');
      exit;
    }
    $this->load->view('admin/login');
  }
}
