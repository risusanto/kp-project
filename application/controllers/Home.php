<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('Document_m');
	}

	public function index()
	{
		$this->data['title'] = 'Home'.$this->title;
		$this->data['content'] = 'home/home';
		$this->template($this->data);
	}

	public function detail_dokumen()
	{
		$id = $this->input->get('id_dokumen');
		if(!isset($id) || $id == NULL ){
			redirect('home');
			exit;
		}
		$this->data['dokumen'] = $this->Document_m->get_row(['id_document' => $id]);
		$this->data['title'] = $this->data['dokumen']->judul.$this->title;
		$this->data['content'] = 'home/dokumen_detail';
		$this->template($this->data);
	}
}
